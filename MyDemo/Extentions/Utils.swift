//
//  Utils.swift
//  MyDemo
//
//  Created by mac on 2021/3/9.
//

import Foundation

public func changeFontColor(totalString: String,
                            subString: String,
                            font: UIFont,
                            textColor: UIColor)-> NSMutableAttributedString {
    let range = (totalString as NSString).range(of: subString)
    let attStr = NSMutableAttributedString.init(string: totalString)
    attStr.addAttributes([NSAttributedString.Key.foregroundColor: textColor,
                          NSAttributedString.Key.font: font], range:range)
    return attStr
}

func documentPath() -> String {
    let documentsPath = NSSearchPathForDirectoriesInDomains( FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first
    let path = documentsPath! + "/user.data"
    return path
}


func setShadow(view:UIView,
               width:CGFloat,
               bColor:UIColor,
               sColor:UIColor,
               offset:CGSize,
               opacity:Float,
               radius:CGFloat) {
        //设置视图边框宽度
        view.layer.borderWidth = width
        //设置边框颜色
        view.layer.borderColor = bColor.cgColor
        //设置边框圆角
        view.layer.cornerRadius = radius
        //设置阴影颜色
        view.layer.shadowColor = sColor.cgColor
        //设置透明度
        view.layer.shadowOpacity = opacity
        //设置阴影半径
        view.layer.shadowRadius = radius
        //设置阴影偏移量
        view.layer.shadowOffset = offset
}
