//
//  LKBaseViewController.swift
//  MyDemo
//
//  Created by mac on 2021/3/4.
//

import UIKit

class LKBaseViewController: UIViewController,MAMapViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    lazy var mapView : MAMapView! = {
  //      AMapServices.shared().apiKey = "b21afa91005b29abbb976617af9ea4f6"
        let mapView = MAMapView(frame: self.view.bounds)
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = MAUserTrackingMode.follow
        return mapView
    }()
    
    func mapViewRequireLocationAuth(_ locationManager: CLLocationManager!) {
        locationManager .requestAlwaysAuthorization()
    }
    
    
}



extension UIImage {
    class func downloadFromURL(url: String) -> UIImage {
        let url = URL(string: url)!;
        var img = UIImage();
        
        do{
            let data = try Data(contentsOf: url);
            img = UIImage(data: data)!;
        } catch let err as NSError {
            print("下载图片出错：\(err)");
        }
        
        return img;
    }
}
