//
//  HomeHeaderCollectionReusableView.swift
//  MyDemo
//
//  Created by mac on 2021/3/8.
//

import UIKit

class HomeHeaderCollectionReusableView: UICollectionReusableView,MAMapViewDelegate {
    var topimgv : UIImageView?
    var label : UILabel?
    var label2 : UILabel?
    var typeSelectView : LKTravelTypeView?
    var count:Int=0{
        didSet(newValue){
            label2?.text = "当地\(count)位推荐官为您服务"
        }
    }
    
    lazy var mapView : MAMapView! = {
    //    AMapServices.shared().apiKey = "b21afa91005b29abbb976617af9ea4f6"
        let mapView = MAMapView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: 200))
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = MAUserTrackingMode.follow
        return mapView
    }()

    required init?(coder: NSCoder) {
        super.init(coder:coder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    func setupUI() -> Void {
        topimgv = UIImageView(image: UIImage(named: "home_background"))
        self .addSubview(topimgv!)
        
        label = UILabel()
        label?.font = UIFont.boldSystemFont(ofSize: 23)
        label?.textColor = UIColor(hex: 0x333333)
        label?.text = "附近推荐官"
        self.addSubview(label!)
        
        label2 = UILabel()
        label2?.font = UIFont.boldSystemFont(ofSize: 14)
        label2?.textColor = UIColor(hex: 0x999999)
        label2?.text = "当地263位推荐官为您服务"
        self.addSubview(label2!)
        
        self.addSubview(mapView)
        mapView.isScrollEnabled = false
        
        typeSelectView = LKTravelTypeView()
        self.addSubview(typeSelectView!)
        
        topimgv?.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: 200)
    
        label?.snp.makeConstraints({ (make) in
            make.top.equalTo(topimgv!.snp_bottomMargin).offset(60)
            make.left.equalTo(18)
            make.height.equalTo(32)
        })
        
        label2?.snp.makeConstraints({ (make) in
            make.top.equalTo(label!.snp_bottomMargin).offset(6)
            make.left.equalTo(18)
            make.height.equalTo(20)
        })
        
        mapView.snp.makeConstraints { (make) in
            make.top.equalTo(label2!.snp_bottomMargin).offset(20)
            make.left.equalTo(18)
            make.rightMargin.equalToSuperview().offset(-20)
            make.height.equalTo(337)
        }
        
        typeSelectView?.snp.makeConstraints({ (make) in
            make.top.equalTo(mapView.snp_bottomMargin).offset(30)
            make.left.equalTo(12)
            make.right.equalTo(-12)
            make.height.equalTo(192)
            
        })
        setShadow(view: typeSelectView!, width: 1, bColor: UIColor.red, sColor: UIColor.gray, offset: CGSize(width: 2, height: 2), opacity: 1.0, radius: 4)
    }
    
    func mapViewRequireLocationAuth(_ locationManager: CLLocationManager!) {
        locationManager .requestAlwaysAuthorization()
    }
    
}
