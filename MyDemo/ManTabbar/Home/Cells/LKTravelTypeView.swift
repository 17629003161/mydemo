//
//  LKTravelTypeView.swift
//  MyDemo
//
//  Created by mac on 2021/3/9.
//

import UIKit

class LKTravelTypeView: UIView {
    var typeLabel : UILabel?
    var typeLabel2 : UILabel?
    var fromLabel : UILabel?
    var toLabel : UILabel?
    var indView : UIView?
    var index: Int = 0 {
        willSet(newNumber) {
            print("About to change to \(newNumber)")
        }
        didSet(oldNumber) {
            print("Just changed from \(oldNumber) to \(self.index)")
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override init(frame: CGRect) {
        super.init(frame:frame)
        setupUI()
    }
    func setupUI() -> Void {
        self.backgroundColor = UIColor.white
        typeLabel = UILabel()
        typeLabel?.font = UIFont.systemFont(ofSize: 15)
        typeLabel?.textColor = UIColor(hex: 0x1E69FF)
        typeLabel?.text = "周边游"
        self.addSubview(typeLabel!)
        
        typeLabel2 = UILabel()
        typeLabel2?.font = UIFont.systemFont(ofSize: 15)
        typeLabel2?.textColor = UIColor(hex: 0x333333)
        typeLabel2?.text = "顺风游"
        self.addSubview(typeLabel2!)
        
        indView = UIView()
        indView?.backgroundColor = UIColor(hex: 0x1E69FF)
        self.addSubview(indView!)

        let x = (ScreenWidth-24)/2.0+33
        typeLabel2?.snp.makeConstraints({ (make) in
            make.top.equalTo(20)
            make.left.equalTo(x)
            make.height.equalTo(16)
        })
        
        typeLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(20)
            make.right.equalTo(typeLabel2!.snp_leftMargin).offset(-66)
            make.height.equalTo(16)
        })
        
        indView?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(typeLabel!)
            make.top.equalTo(typeLabel!.snp_bottomMargin).offset(13.5)
            make.height.equalTo(1)
            make.width.equalTo(21)
        })
    
    }

}
