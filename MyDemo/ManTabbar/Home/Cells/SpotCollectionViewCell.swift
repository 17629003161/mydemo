//
//  SpotCollectionViewCell.swift
//  MyDemo
//
//  Created by mac on 2021/3/9.
//

import UIKit

class SpotCollectionViewCell: UICollectionViewCell {
    var spotImgv : UIImageView?
    var locImgv : UIImageView?
    var nameLable : UILabel?
    var descLabel : UILabel?
    var imagAry : [UIImageView]?
    var totolLabel : UILabel?
    var model:Datum?{
        willSet(newValue){
            nameLable?.text = newValue?.scenicName
            descLabel?.text = newValue?.scenicAbstract
            if let url = URL(string: newValue!.scenicImage) {
                spotImgv!.downloadedFrom(url: url)
            }
            totolLabel?.text = "共" + "\(newValue!.ltRefUserInfos.count)" + "名推荐管支持服务"
            let ary = newValue?.ltRefUserInfos;
            for idx in 0..<imagAry!.count{
                let imgv:UIImageView = imagAry![idx]
                if(idx > newValue!.ltRefUserInfos.count){
                    imgv.isHidden = true
                }else{
                    imgv.isHidden = false
                    let md :LtRefUserInfo = ary![idx]
                    if let url = URL(string: md.headerImage){
                        imgv.downloadedFrom(url: url)
                    }
                }
            }

        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    func setupUI() -> Void {
        setShadow(view: self, width: 1, bColor: UIColor.gray, sColor: UIColor.gray, offset: CGSize(width: 2, height: 2), opacity: 1.0, radius: 6)
        imagAry = Array()
        spotImgv = UIImageView(image: UIImage(named: "0"))
        self.addSubview(spotImgv!)
        locImgv = UIImageView(image: UIImage(named: "address_Location"))
        self.addSubview(locImgv!)
        nameLable = UILabel()
        nameLable?.font = UIFont.systemFont(ofSize: 10)
        nameLable?.textColor = UIColor(hex: 0x959393)
        nameLable?.text = "西安·大雁塔"
        self.addSubview(nameLable!)
        descLabel = UILabel()
        descLabel?.font = UIFont.systemFont(ofSize: 13)
        descLabel?.textColor = UIColor(hex: 0x414243)
        descLabel?.numberOfLines = 2
        descLabel?.text = "玄奘为保存由天竺经丝绸之路带回长安的经书修带回长安的经书修"
        self.addSubview(descLabel!)
        totolLabel = UILabel()
        totolLabel?.font = UIFont.systemFont(ofSize: 10)
        totolLabel?.textColor = UIColor(hex: 0x999999)
        totolLabel?.text = "共28名推荐管支持服务"
        self.addSubview(totolLabel!)
        for i in 0..<3 {
            var imgv : UIImageView?
            imgv = UIImageView(image:UIImage(named: "0"))
            imgv?.layer.borderColor = UIColor.white.cgColor
            imgv?.layer.borderWidth = 1.0
            imgv?.layer.cornerRadius = 10
            imgv?.layer.masksToBounds = true
            imagAry?.append(imgv!)
            self.addSubview(imgv!)
            
            imgv?.snp.makeConstraints({ (make) in
                make.top.equalTo(descLabel!.snp.bottom).offset(8)
                make.left.equalTo(6+i*8)
                make.width.equalTo(20)
                make.height.equalTo(20)
            })
        }
        
        spotImgv?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self)
            make.height.equalTo(107)
        })
        
        locImgv?.snp.makeConstraints({ (make) in
            make.top.equalTo(spotImgv!.snp.bottom).offset(8)
            make.left.equalTo(6)
            make.width.equalTo(9.82)
            make.height.equalTo(12.55)
        })
        
        nameLable?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(locImgv!)
            make.left.equalTo(locImgv!.snp.right).offset(6)
            make.right.equalTo(-6)
            make.height.equalTo(14)
        })
        
        descLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(6)
            make.top.equalTo(nameLable!.snp.bottom).offset(8)
            make.right.equalTo(-6)
        })
        
        totolLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(descLabel!.snp.bottom).offset(11)
            make.left.equalTo(55)
            make.right.equalTo(-6)
        })
        
        
        
    }
}
