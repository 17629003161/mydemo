//
//  PriceRulesModel.swift
//  TravelNew
//
//  Created by mac on 2021/4/14.
//  Copyright © 2021 lester. All rights reserved.
//

import Foundation

// MARK: - Welcome

struct PriceRulesModel: Codable {
    let msg: String
    let code: Int
    let data: [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let scenicID: String
    let latitude, longitude: Double
    let hotVar: Int
    let scenicName: String
    let scenicImage: String
    let scenicAbstract, city: String
    let price: Int
    let ltRefUserInfos: [LtRefUserInfo]
    let ltRefUserNum, sequence: Int

    enum CodingKeys: String, CodingKey {
        case scenicID = "scenicId"
        case latitude, longitude, hotVar, scenicName, scenicImage, scenicAbstract, city, price, ltRefUserInfos, ltRefUserNum, sequence
    }
}

// MARK: - LtRefUserInfo
struct LtRefUserInfo: Codable {
    let userID: Int
    let userNickname: String
    let headerImage: String
    let referrerLatitude, referrerLongitude: Double
    let scenicID: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case userNickname, headerImage, referrerLatitude, referrerLongitude
        case scenicID = "scenicId"
    }
}
