//
//  ViewController1.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import UIKit
import Alamofire

class ViewController1: UIViewController {
    var topImgv : UIImageView?
    var textImgv : UIImageView?
    var dataArray :[Datum] = Array()
    var count : Int = 0
    lazy var colView: UICollectionView! = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 13
        layout.minimumInteritemSpacing = 2
        layout.sectionInset = UIEdgeInsets(top: 0, left: 18, bottom: 0, right:18)
        //设置组头的大小
        layout.headerReferenceSize = CGSize(width: ScreenWidth-32, height: 44)
        layout.footerReferenceSize = CGSize(width: ScreenWidth-32, height: 16)
        
        let colView = UICollectionView(frame: CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight), collectionViewLayout: layout);
        colView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        colView.backgroundColor = UIColor.white
        colView.showsVerticalScrollIndicator = false
        colView.delegate = self
        colView.dataSource = self
        colView.register(SpotCollectionViewCell.self, forCellWithReuseIdentifier: "SpotCollectionViewCell")
        colView.register(HomeHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HomeHeaderCollectionReusableView")
        colView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "UICollectionReusableView")
        return colView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addSubview(colView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        requestData()
    }
    
    func requestData() -> Void {
        let queue = DispatchQueue.global()
        let group = DispatchGroup()
        group.enter()
        queue.async(group: group) {
            let url = BaseUrl + "attractions/scenic/list"
            let para : [String:Any] = ["cityid":"029",
                                       "pageIndex":1,
                                       "pageSize":4]
            AF.request(url, method: .post, parameters: para).responseJSON{(response) in
                switch response.result {
                case .success:
                    let jsonstr = String(bytes: response.data!, encoding: .utf8)!
                    print("\(jsonstr)")
                    if let json = jsonstr.data(using: .utf8) {
                        let model = try? JSONDecoder().decode(PriceRulesModel.self, from:json)
                        self.dataArray.removeAll()
                        self.dataArray += model!.data;
                    }
                    group.leave()
                case .failure(let error):
                    print("error:\(error)")
                    group.leave()
                }
            }
        }
        group.enter()
        queue.async(group: group){
            let url2 = BaseUrl + "user/LtReferrerUser/getLtReferrerNum"
            let para2 = ["cityid":"029"]
            AF.request(url2, method: .post, parameters: para2).responseJSON{(response) in
                switch response.result {
                case .success(let json):
                    let dict:Dictionary  = json as! [String:AnyObject]
                    let count :Int = dict["data"] as! Int
                    self.count = count
                    print("count = \(count)")
                    let jsonstr = String(bytes: response.data!, encoding: .utf8)!
                    print("\(jsonstr)")
                    group.leave()
                case .failure(let error):
                    print("error:\(error)")
                    group.leave()
                }
            }
        }
        group.notify(queue: queue){
            print("All task has finished")
            DispatchQueue.main.async {
                //刷新ui
                self.colView .reloadData()
            }
            
        }
    }
}



extension ViewController1:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return  CGSize(width: ScreenWidth, height: 922)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
            return CGSize(width: ScreenWidth, height: 130)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = (ScreenWidth-49)/2.0
            return CGSize(width: width, height: 212)
    }
    


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpotCollectionViewCell", for: indexPath) as! SpotCollectionViewCell
        cell.model = self.dataArray[indexPath.item]
        cell.backgroundColor = UIColor.white
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if(kind == UICollectionView.elementKindSectionHeader){
            let headerView :HomeHeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaderCollectionReusableView", for: indexPath) as! HomeHeaderCollectionReusableView
            headerView.count = self.count
            return headerView
        }else{
            let footerView : UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "UICollectionReusableView", for: indexPath)
            footerView.subviews.forEach({ $0.removeFromSuperview() })
            return footerView
        }
    }

    // 单击单元格时，跳转到详情页面
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.section==0){
            if(indexPath.item==0){
                let vc = NextViewController()
                vc.data1 = "hjx"
                vc.view.backgroundColor = UIColor.red
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    

    
    
}


