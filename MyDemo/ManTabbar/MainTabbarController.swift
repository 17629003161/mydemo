//
//  MainTabbarController.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import UIKit

class MainTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //设置系统默认的蓝色
        //self.tabBar.tintColor = UIColor(red: 0/255, green:169/255, blue:169/255, alpha:1)
        addNormalTabbar()
    }
    
    // MARK: -FirstNormalTabba 第一种常用的tabbar
    /**
     优点可以拿到单独子控制器 做相应的设置
     */
    func addNormalTabbar() {
        setupOneChildViewController(title: "首页", image: "icon_home", seletedImage: "icon_home_select", controller: ViewController1.init())
        setupOneChildViewController(title: "需求", image: "icon_demand", seletedImage: "icon_demand_select", controller: ViewController2.init())
        setupOneChildViewController(title: "消息", image: "icon_message", seletedImage: "icon_message_select", controller: ViewController3.init())
        setupOneChildViewController(title: "我的", image: "icon_my", seletedImage: "icon_my_select", controller: ViewController4.init())
    }
    fileprivate func  setupOneChildViewController(title: String,image: String,seletedImage: String,controller: UIViewController){
        controller.tabBarItem.title = title
        controller.title = title
        //这里设置背景色 是每一个vc设置的都一样
        controller.view.backgroundColor = UIColor.white
        controller.tabBarItem.image = UIImage.init(named: image)
        controller.tabBarItem.selectedImage = UIImage.init(named: seletedImage)
        let navVC = UINavigationController(rootViewController: controller)
        self.addChild(navVC)
    }

}
