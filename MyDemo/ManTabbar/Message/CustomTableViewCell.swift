//
//  CustomTableViewCell.swift
//  MyDemo
//
//  Created by mac on 2021/3/1.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    var leftImageView : UIImageView?
    var nameLabel : UILabel?
    var subNameLabel : UILabel?
    var timeLabel : UILabel?
    var browseLabel : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style:style, reuseIdentifier:reuseIdentifier )
        self.createUI()
    }
    
    func createUI() -> Void {
        leftImageView = UIImageView()
        leftImageView!.backgroundColor = UIColor.lightGray
        leftImageView?.image = UIImage(named: "background")
        self.contentView.addSubview(leftImageView!)
        
        nameLabel = UILabel()
        nameLabel?.textColor = UIColor.darkText
        nameLabel?.font = UIFont.systemFont(ofSize: 18)
        nameLabel?.text = "世界杯开幕";
        self.contentView.addSubview(nameLabel!)
        
        subNameLabel = UILabel.init()
        subNameLabel?.textColor = UIColor.darkGray
        subNameLabel?.font = UIFont.systemFont(ofSize: 15)
        subNameLabel?.text = "世界杯开幕";
        self.contentView.addSubview(subNameLabel!)
         
        timeLabel = UILabel.init()
        timeLabel?.textColor = UIColor.lightGray
        timeLabel?.font = UIFont.systemFont(ofSize: 13)
        timeLabel?.text = "2018-01-01 10:58"
        self.contentView.addSubview(timeLabel!)
         
        browseLabel = UILabel.init()
        browseLabel?.textAlignment = NSTextAlignment.right
        browseLabel?.textColor = UIColor.lightGray
        browseLabel?.font = UIFont.systemFont(ofSize: 13)
        browseLabel?.text = "浏览："+"50"
        self.contentView.addSubview(browseLabel!)
        
        leftImageView?.snp.makeConstraints({ (make) in
            make.top.equalTo(10)
            make.left.equalTo(10)
            make.width.equalTo(120)
            make.height.equalTo(80)
        })
        
        nameLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(leftImageView!.snp_topMargin)
            make.left.equalTo(leftImageView!.snp_rightMargin).offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(18)
        })
        
        subNameLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(nameLabel!.snp_bottomMargin).offset(10)
            make.left.equalTo(nameLabel!)
            make.right.equalTo(nameLabel!)
            make.height.equalTo(18)
        })
        
        timeLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(subNameLabel!.snp_bottomMargin).offset(10)
            make.left.equalTo(subNameLabel!)
            make.right.equalTo(160)
            make.height.equalTo(18)
        })
        
        browseLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(timeLabel!)
            make.right.equalToSuperview().offset(-20)
            make.width.equalTo(160)
            make.height.equalTo(18)
        })
        
        

        
        
        
        
        
        
    }

}
