//
//  NextViewController.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import UIKit

class NextViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var data1: String?
    var tableView:UITableView?;

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print("\(data1!)")
//
//        func minMax(array: [Int]) -> (min: Int, max: Int) {
//            var currentMin = array[0]
//            var currentMax = array[0]
//            for value in array[1..<array.count] {
//                if value < currentMin {
//                    currentMin = value
//                } else if value > currentMax {
//                    currentMax = value
//                }
//            }
//            return (currentMin, currentMax)
//        }
//
//        let bounds = minMax(array: [8, -6, 2, 109, 3, 71])
//        print("最小值为 \(bounds.min) ，最大值为 \(bounds.max)")
        
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //隐藏状态栏
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupTableView() {
            tableView = UITableView(frame: view.bounds, style: .plain)
            tableView?.delegate = self;
            tableView?.dataSource = self;
            tableView?.register(TYCustomCell.self, forCellReuseIdentifier: "TYCustomCell")
            view.addSubview(tableView!)
            tableView?.estimatedRowHeight = 44.0
        tableView?.rowHeight = UITableView.automaticDimension
            
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 5
        }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TYCustomCell", for: indexPath) as! TYCustomCell
    //        cell.textLabel?.text = "xxxx\(indexPath.row)"
            cell.titleLab?.text = "我是假数据"
            if indexPath.row%2==0 {
                cell.despLab?.text = "我是假数据我是假数据我是假数据"
            }
            else
            {
                if indexPath.row%3 == 0{
                    cell.despLab?.text = "我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据"
                }
                else{
                    cell.despLab?.text = "我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据我是假数据"
                }
            }
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            print("第\(indexPath.row)行被点击了")
        }
    
    
}
