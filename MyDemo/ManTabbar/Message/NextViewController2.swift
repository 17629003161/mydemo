//
//  NextViewController2.swift
//  MyDemo
//
//  Created by mac on 2021/3/9.
//

import UIKit

class NextViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUI()
        
    }
    
    func setUI() -> Void {
        let scrollView = UIScrollView()
        let contentView = UIView()
        contentView.backgroundColor = UIColor.green
        let view1 = UIView()
        view1.backgroundColor = UIColor.red
        let view2 = UIView()
        view2.backgroundColor = UIColor.blue
                
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(view1)
        contentView.addSubview(view2)
                
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
                
        contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(scrollView)
            make.width.equalTo(view)
        }
                
        view1.snp.makeConstraints { (make) in
            make.top.equalTo(contentView)
            make.leading.trailing.equalTo(contentView)
            make.width.equalTo(contentView)
            make.height.equalTo(500)
        }
                
        view2.snp.makeConstraints { (make) in
            make.top.equalTo(view1.snp.bottom)
            make.bottom.equalTo(contentView.snp.bottom)
            make.leading.trailing.equalTo(contentView)
            make.width.equalTo(contentView)
            make.height.equalTo(500)
        }
    }
    func test() -> Void {
        let user : UserInfo! = UserInfo.sharedInstance
        let name = user.name ?? ""
        print("\(name)")

        let label = UILabel()
        label.attributedText = changeFontColor(totalString: "我是13号字体黑色我是17号加粗字体红色", subString: "我是17号", font: UIFont.systemFont(ofSize: 17), textColor: UIColor.red)
        self.view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.center.equalTo(view)
            make.height.equalTo(30)
        }
    }

}
