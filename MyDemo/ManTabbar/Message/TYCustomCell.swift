//
//  TYCustomCell.swift
//  MyDemo
//
//  Created by mac on 2021/3/2.
//

import UIKit

class TYCustomCell: UITableViewCell {
    var imgView: UIImageView?
    var titleLab:UILabel?
    var despLab:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder: NSCoder) {
        super .init(coder: coder)
        setupUI()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    func setupUI(){
        //初始化头像
        imgView = UIImageView()
        imgView?.image = UIImage.init(named: "background")
        imgView?.layer.borderColor = UIColor.gray.cgColor
        imgView?.layer.borderWidth = 1.0
        self.contentView.addSubview(imgView!)
        
        //顶部的label 初始化
        titleLab = UILabel()
        titleLab?.font = .systemFont(ofSize: 15)
        titleLab?.text = "test is ok"
        titleLab?.textColor = .red
        self.contentView.addSubview(titleLab!)
        
        //底部的label 初始化
        let label2 = UILabel()
        label2.font = .systemFont(ofSize: 14)
        label2.textColor = .black
        label2.numberOfLines = 0
        self.contentView.addSubview(label2)
        self.despLab = label2;
        
        //设置布局 SnapKit  --- >相当去Objective-C中的Masonry
        imgView?.snp.makeConstraints({ (make) in
            make.top.left.equalTo(10)
            make.width.height.equalTo(40)
        })
        
        titleLab!.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo((imgView?.snp.right)!).offset(10)
            make.right.equalTo(-10)
            make.height.equalTo(21)
        }
        
        label2.snp.makeConstraints { (make) in
            make.top.equalTo(titleLab!.snp.bottom).offset(10)
            make.left.equalTo((imgView?.snp.right)!).offset(10)
            make.right.equalTo(-10)
            make.bottom.equalTo(-10)
        }
    }
}
