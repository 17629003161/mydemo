//
//  TestViewController.swift
//  MyDemo
//
//  Created by mac on 2021/4/8.
//

import UIKit

enum Section: String, Codable
{
    case A
    case B
    case C
}
class Student: NSObject, Codable
{
    var name: String = ""
    var id: URL? = nil
    var year: Int = 0
    var isNew:Bool = true
    var peer: [String:String]? = nil
    var section: Section = .A
 
}

class TestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        test2()
    }
    
    func test() -> Void {
        let student = Student()
        student.name = "Anupam"
        student.year = 2011
        student.id = URL(string: "https://www.journaldev.com")
        student.section = .B
         
        let encodedObject = try? JSONEncoder().encode(student)
        if let encodedObjectJsonString = String(data: encodedObject!, encoding: .utf8)
        {
            print(encodedObjectJsonString)
        }
    }
    
    func test2() -> Void {
        let jsonString = """
        {
        "name":"Anupam",
        "isNew":true,
        "year":2018,
        "id":"j.com",
        "section":"A"
        }
        """
        if let jsonData = jsonString.data(using: .utf8)
        {
            let studentObject = try? JSONDecoder().decode(Student.self, from: jsonData)
            print(studentObject?.name);
        }

    }
    
}
