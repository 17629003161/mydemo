//
//  ViewController3.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import UIKit
import CLToast


class ViewController3: UIViewController {
    var imageView: UIImageView!
    var btn:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI()  {
        //初始化table，为了简化代码，frame为全屏尺寸
        let table:UITableView = UITableView(frame:view.bounds, style: .plain)
        table.register(CustomTableViewCell.self, forCellReuseIdentifier: "cellID")
        //table添加到视图上，并声明delegate和datasource
        self.view!.addSubview(table)
        table.dataSource = self
        table.delegate = self
       // table.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
        

        
    }
}


extension ViewController3: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 20
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! CustomTableViewCell
        cell.nameLabel!.text = "\(indexPath.row)"
        cell.subNameLabel?.text = "sub name label"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = NextViewController()
            vc.data1 = "hjx"
            vc.view.backgroundColor = UIColor.red
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = NextViewController2()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
}
