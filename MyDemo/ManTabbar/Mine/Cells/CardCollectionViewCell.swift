//
//  CardCollectionViewCell.swift
//  MyDemo
//
//  Created by mac on 2021/3/8.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    var bakimgv :UIImageView?
    var label1 : UILabel?
    var label2 : UILabel?

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    func setupUI() -> Void {
        self.backgroundColor = UIColor.white
        bakimgv = UIImageView()
        bakimgv?.image = UIImage(named: "card_background")
        label1 = UILabel()
        label1?.font = UIFont.systemFont(ofSize: 14)
        label1?.textColor = UIColor(hex: 0x333333)
        label1?.text = "0.00元"
        label2 = UILabel()
        label2?.font = UIFont.systemFont(ofSize: 10)
        label2?.textColor = UIColor(hex: 0x333333)
        label2?.text = "我的钱包"
        self.addSubview(bakimgv!)
        bakimgv!.addSubview(label1!)
        bakimgv!.addSubview(label2!)
        bakimgv?.snp.makeConstraints({ (make) in
            make.left.equalToSuperview().offset(15)
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-8)
            make.bottom.equalToSuperview().offset(-20)
        })
        label1?.snp.makeConstraints({ (make) in
            make.top.equalTo(10)
            make.left.equalTo(15)
            make.height.equalTo(20)
        })
        label2?.snp.makeConstraints({ (make) in
            make.top.equalTo(label1!.snp_bottomMargin).offset(10)
            make.left.equalTo(15)
            make.height.equalTo(14)
        })
        
    }
}
