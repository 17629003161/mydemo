//
//  CustomCollectionViewCell.swift
//  MyDemo
//
//  Created by mac on 2021/3/3.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    var id: Int = 0;   // 用于记录每个记录的ID
    
    var imageView: UIImageView?
    var titleLabel: UILabel?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = UIImageView()
        imageView?.contentMode = .scaleAspectFill
        imageView?.clipsToBounds = true
        self.addSubview(imageView!)
        
        titleLabel = UILabel()
        titleLabel!.textAlignment = .center
        titleLabel?.font = UIFont(name: "Heiti SC", size: 11)
        self.addSubview(titleLabel!)
        
        imageView?.snp.makeConstraints({ (make) in
            make.size.equalTo(CGSize(width: 28, height:28))
            make.centerX.equalTo(self)
            make.centerY.equalTo(self).offset(-10)
        })
        
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self).offset(17)
            make.width.equalTo(50)
            make.height.equalTo(20)
        })
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("Cell 初始化失败")
    }
}
