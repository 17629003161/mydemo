//
//  FooterCollectionReusableView.swift
//  MyDemo
//
//  Created by mac on 2021/3/8.
//

import UIKit

class FooterCollectionReusableView: UICollectionReusableView {
    var btn : UIButton?
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    func setupUI() -> Void {
        self.backgroundColor = UIColor.clear
        btn = UIButton(type: .custom)
        btn?.setTitle("当推荐管挣钱", for: .normal)
        btn?.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn?.setTitleColor(UIColor(hex: 0x333333), for: .normal)
        btn?.backgroundColor = UIColor(hex: 0xEEEEEE)
        btn?.layer.cornerRadius = 20
        btn?.layer.masksToBounds = true
        self.addSubview(btn!)
        
        btn?.snp.makeConstraints({ (make) in
            make.center.equalTo(self)
            make.width.equalTo(213)
            make.height.equalTo(40)
        })
    }
}
