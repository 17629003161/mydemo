//
//  HeaderCollectionReusableView.swift
//  MyDemo
//
//  Created by mac on 2021/3/5.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    var leftLabel : UILabel?
    var rightLabel : UILabel?
    var arrow :UIImageView?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    func setupUI() -> Void {
        self.backgroundColor = UIColor.white
        leftLabel = UILabel()
        leftLabel?.font = UIFont.systemFont(ofSize: 16)
        leftLabel?.textColor = UIColor.init(hex: 0x333333)
        leftLabel?.text = "我的订单"
        self.addSubview(leftLabel!)
        
        rightLabel = UILabel()
        rightLabel?.font = UIFont.systemFont(ofSize: 14)
        rightLabel?.textColor = UIColor(hex: 0x999999)
        rightLabel?.text = "全部订单"
        self.addSubview(rightLabel!)
        
        arrow = UIImageView(image: UIImage.init(named:"arriw_right_gay"))
        self.addSubview(arrow!)
        
        leftLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(self).offset(20)
            make.top.equalTo(self).offset(14)
            make.height.equalTo(18)
        })
        
        rightLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(leftLabel!)
            make.right.equalTo(self).offset(-31)
            make.height.equalTo(18)
        })
        
        arrow?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(leftLabel!)
            make.right.equalTo(self).offset(-20)
            make.width.equalTo(6)
            make.height.equalTo(13.8)
        })
    }
    
    
    
}
