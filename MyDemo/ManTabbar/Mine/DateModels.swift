//
//  DateModels.swift
//  MyDemo
//
//  Created by mac on 2021/3/10.
//


import Foundation
import HandyJSON

class BaseResponse<T: HandyJSON>: HandyJSON {
    var code: Int? // 服务端返回码
    var data: T? // 具体的data的格式和业务相关，故用泛型定义
    var msg: String?
    public required init() {}
}
 
// 假设这是某一个业务具体的数据格式定义
struct LoginData: HandyJSON {
    var token: String?
    var userId: String?
}
