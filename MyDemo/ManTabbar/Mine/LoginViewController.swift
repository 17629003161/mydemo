//
//  LoginViewController.swift
//  MyDemo
//
//  Created by mac on 2021/2/25.
//

import UIKit
import Alamofire
import SnapKit
import CLToast
import HandyJSON

class LoginViewController: UIViewController {

    var phoneField : UITextField?
    var vercodeField : UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setupUI() -> Void {
        let backArrow = UIImageView(image: UIImage(named: "icon-back"))
        self.view.addSubview(backArrow)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(tap:)))
        backArrow.isUserInteractionEnabled = true
        backArrow.addGestureRecognizer(tap)
        
        let label1 = UILabel()
        label1.text = "请填写以下登录信息"
        label1.textColor = UIColor(hex: 0x2C2C2C)
        label1.font = UIFont.systemFont(ofSize: 18)
        self.view.addSubview(label1)
        
        let label2 = UILabel()
        label2.text = "使用快捷登录更便捷"
        label2.textColor = UIColor(hex: 0xCACACA)
        label2.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(label2)
        
        
        let phone = UIImageView(image: UIImage(named: "shouji"))
        self.view .addSubview(phone)
        
        let plabel1 = UILabel()
        plabel1.text = "手机号"
        plabel1.textColor = UIColor(hex: 0x333333)
        plabel1.font = UIFont.systemFont(ofSize: 14)
        self.view .addSubview(plabel1)
        
        let phoneField = UITextField()
        phoneField.placeholder = "请输入您的手机号"
        phoneField.font = UIFont.systemFont(ofSize: 13)
        phoneField.textColor = UIColor(hex: 0x999999)
        self.view .addSubview(phoneField)
        self.phoneField = phoneField;
    
        let line = UIView()
        line.backgroundColor = UIColor(hex: 0xE7E7E7)
        self.view.addSubview(line)
        
        
        let yzm = UIImageView(image: UIImage(named: "yanzhengma"))
        self.view .addSubview(yzm)
        
        let plabel2 = UILabel()
        plabel2.text = "验证码"
        plabel2.textColor = UIColor(hex: 0x333333)
        plabel2.font = UIFont.systemFont(ofSize: 14)
        self.view .addSubview(plabel2)
        
        let verField = UITextField()
        verField.placeholder = "请输入您收到的验证码"
        verField.font = UIFont.systemFont(ofSize: 13)
        verField.textColor = UIColor(hex: 0x999999)
        self.view .addSubview(verField)
        self.vercodeField = verField
        
        let btn1 = UIButton(type: .custom)
        btn1.setTitle("获取验证码", for: .normal)
        btn1.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        btn1.setTitleColor(UIColor(hex: 0x333333), for: .normal)
        btn1.layer.borderWidth = 1
        btn1.layer.borderColor = UIColor(hex: 0xE7E7E7)?.cgColor
        btn1.layer.cornerRadius = 15
        btn1.layer.masksToBounds = true
        btn1.addTarget(self, action: #selector(sendCodeBtnIsClicked(btn:)), for: .touchUpInside)
        self.view.addSubview(btn1)
        
        let line2 = UIView()
        line2.backgroundColor = UIColor(hex: 0xE7E7E7)
        self.view.addSubview(line2)
        
        let color = UIColor(hex: 0x4D9BFD)
        
        let btn2 = UIButton(type: .custom)
        btn2.setTitle("登录", for: .normal)
        btn2.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        btn2.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
        btn2.setBackgroundImage(UIImage(color: color!), for: .normal)
        btn2.layer.borderWidth = 1
        btn2.layer.borderColor = UIColor(hex: 0xE7E7E7)?.cgColor
        btn2.layer.cornerRadius = 22
        btn2.layer.masksToBounds = true
        btn2.addTarget(self, action: #selector(loginIsClicked), for: .touchUpInside)
        self.view.addSubview(btn2)
        
        
        
        backArrow.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(30)
            make.left.equalTo(self.view).offset(10)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        
        label1.snp.makeConstraints { (make) in
            make.top.equalTo(backArrow.snp_bottomMargin).offset(21)
            make.left.equalToSuperview().offset(36)
            make.height.equalTo(28)
        }
        
        label2.snp.makeConstraints { (make) in
            make.top.equalTo(label1.snp_bottomMargin).offset(10)
            make.left.equalTo(label1)
        }
        
        phone.snp.makeConstraints { (make) in
            make.top.equalTo(label2.snp_bottomMargin).offset(57)
            make.left.equalToSuperview().offset(40)
            make.width.equalTo(12.73)
            make.height.equalTo(20)
        }
        
        plabel1.snp.makeConstraints { (make) in
            make.centerY.equalTo(phone)
            make.left.equalToSuperview().offset(65)
            make.height.equalTo(20)
        }
        
        phoneField.snp.makeConstraints { (make) in
            make.top.equalTo(plabel1.snp_bottomMargin).offset(13)
            make.left.equalToSuperview().offset(66)
            make.height.equalTo(18)
        }
        
        line.snp.makeConstraints { (make) in
            make.top.equalTo(phoneField.snp_bottomMargin).offset(17)
            make.left.equalToSuperview().offset(36)
            make.right.equalToSuperview().offset(-36)
            make.height.equalTo(1)
        }
        
        yzm.snp.makeConstraints { (make) in
            make.top.equalTo(line).offset(23)
            make.left.equalTo(phone)
            make.width.equalTo(13.99)
            make.height.equalTo(16.14)
        }
        
        plabel2.snp.makeConstraints { (make) in
            make.centerY.equalTo(yzm)
            make.left.equalToSuperview().offset(65)
            make.height.equalTo(20)
        }
        
        verField.snp.makeConstraints { (make) in
            make.top.equalTo(plabel2.snp_bottomMargin).offset(13)
            make.left.equalToSuperview().offset(66)
            make.height.equalTo(18)
        }
        
        btn1.snp.makeConstraints { (make) in
            make.centerY.equalTo(verField)
            make.right.equalTo(line)
            make.width.equalTo(96)
            make.height.equalTo(30)
        }
        
        line2.snp.makeConstraints { (make) in
            make.top.equalTo(btn1.snp_bottomMargin).offset(12)
            make.left.equalTo(line)
            make.right.equalTo(line)
            make.height.equalTo(1)
        }
        
        btn2.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(line2.snp_bottomMargin).offset(50)
            make.left.equalToSuperview().offset(28)
            make.right.equalToSuperview().offset(-28)
            make.height.equalTo(44)
        }
        
    }
    
    @objc func handleTap(tap: UITapGestureRecognizer) {
        if tap.state == .ended {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func sendCodeBtnIsClicked(btn:UIButton?) -> Void {
        let phoneNumber = self.phoneField?.text
        if(phoneNumber?.count==0){
            print("请输入手机号")
            CLToast.cl_show(msg: "请输入手机号")
            return
        }
        print("\(phoneNumber!)");
        let url = "https://www.sxlkcx.cn/travel/user/getValidate"
        AF.request(url, method: .post, parameters: ["phone":phoneNumber]).responseJSON{(response) in
            switch response.result {
            case .success:
                let jsonstr = String(bytes: response.data!, encoding: .utf8)!
                print("\(jsonstr)")
                if let json = jsonstr.data(using: .utf8) {
                    let model = try? JSONDecoder().decode(Welcome1.self, from:json)
                    print(model!)
                    CLToast.cl_show(msg: model!.msg)
                }
                break
            case .failure(let error):
                print("error:\(error)")
                break
            }
        }
    }
    
    @objc func loginIsClicked(){
        let user : UserInfo = UserInfo.sharedInstance
        user.userId = "1970305000001"
        user.name = "hjx"
        user.sex = "男"
        let phoneNumber = self.phoneField?.text
        let vercode = self.vercodeField?.text
        if(phoneNumber?.count==0){
            CLToast.cl_show(msg: "请输入手机号")
            return
        }
        if(vercode?.count==0){
            CLToast.cl_show(msg: "请输入验证码")
            return
        }
        let url = "https://www.sxlkcx.cn/travel/user/register"
        let para = ["phone":phoneNumber,"validate":vercode]
        AF.request(url, method: .post, parameters:para).responseJSON { (response) in
            switch response.result {
            case .success(let json):
                let jsonstr = String(bytes: response.data!, encoding: .utf8)!
                print("\(jsonstr)")
                if let json = jsonstr.data(using: .utf8) {
                    let model = try? JSONDecoder().decode(Welcome.self, from:json)
                    CLToast.cl_show(msg: model!.msg)
                }
//                if let object = Welcome2.deserialize(from: jsonstr){
//                    print(object)
//                }
                break
            case .failure(let error):
                print("error:\(error)")
                break
            }
        }
    }
}
