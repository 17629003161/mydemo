//
//  ViewController4.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import UIKit

class ViewController4: UIViewController {
    var headerImageView : UIImageView?
    var nameLabel : UILabel?
    var funArray : Array<Array<String>>?
    var mainImgv : UIImageView?
    var topImageView : UIImageView!
    var bgview : UIView?
    
    var isCustom : Bool? = true{
        didSet{
            self.funArray?.removeAll()
            if(isCustom==true){
                self.funArray?.append(contentsOf: itemAry2)
            }else{
                self.funArray?.append(contentsOf: itemAry3)
            }
            self.colView .reloadData()
        }
    }

    let itemAry0 = [["icon_baojia","沟通中"],["icon_out","服务中"],["icon_pingjia","已完成"]]
    let itemAry2 = [["icon_publish","我的发布"],["icon_setup","系统设置"],["icon_safe","安全中心"],
                    ["icon_kefu","客服中心"],["icon_zhinan","使用指南"],["icon_heart","我的收藏"],
                    ["icon_step","我的足迹"]]
   
    let itemAry3 = [["open_service","开通服务"],["icon_setup","主页设置"],["icon_advice","系统设置"],
                    ["icon_safe","安全中心"],["icon_kefu","客服中心"],["icon_yaoqing","成为合伙人"],
                    ["icon_heart","我的收藏"],["icon_zhinan","服务指南"],["icon_step","我的足迹"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hex: 0xF5F5F7)
        // Do any additional setup after loading the view.
        self.funArray = Array()
        self.isCustom = true
        setupUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setupUI(){
        bgview = UIView()
        bgview?.backgroundColor = UIColor(hex: 0xF5F5F7)
        self.view.addSubview(bgview!)
        
        bgview?.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalToSuperview()
        }
        
        topImageView = UIImageView();
        topImageView.image = UIImage(named: "image_top_mine")
        self.view.addSubview(topImageView)
        
        headerImageView = UIImageView()
        headerImageView?.image = UIImage(named: "header_default")
        headerImageView?.layer.borderColor = UIColor.white.cgColor
        headerImageView?.layer.cornerRadius = 21
        headerImageView?.layer.masksToBounds = true
        headerImageView?.layer.borderWidth = 1.0
        self.view.addSubview(headerImageView!)
        
        nameLabel = UILabel()
        nameLabel?.font = UIFont.systemFont(ofSize: 20)
        nameLabel?.textColor = UIColor.white
        nameLabel?.text = "注册/登录"
        nameLabel?.isUserInteractionEnabled = true
        let tapGes = UITapGestureRecognizer(target: self, action:#selector(self.loginIsTaped(tapGes: )))
        nameLabel?.addGestureRecognizer(tapGes)
                                            
        self.view.addSubview(nameLabel!)
        
        mainImgv = UIImageView(image: UIImage(named: "mine_page"))
        self.view.addSubview(mainImgv!)
        topImageView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(200)
        }
        
        headerImageView?.snp.makeConstraints({ (make) in
            make.top.equalTo(84)
            make.left.equalTo(34)
            make.width.equalTo(42)
            make.height.equalTo(42)
        })
        
        nameLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(headerImageView!)
            make.leftMargin.equalTo(headerImageView!.snp_rightMargin).offset(20)
            make.height.equalTo(24)
        })
        
        mainImgv?.snp.makeConstraints { (make) in
            make.centerY.equalTo(nameLabel!)
            make.rightMargin.equalToSuperview()
            make.width.equalTo(84)
            make.height.equalTo(28)
        }
        view .addSubview(colView)
    }
    
    lazy var colView: UICollectionView! = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = (ScreenWidth-32)/3.0
        layout.itemSize = CGSize(width: width, height: 82)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0)
        
        //设置组头的大小
        layout.headerReferenceSize = CGSize(width: ScreenWidth-32, height: 44)
        layout.footerReferenceSize = CGSize(width: ScreenWidth-32, height: 16)
        
        let colView = UICollectionView(frame: CGRect(x: 16, y: 0, width: ScreenWidth-32, height: ScreenHeight), collectionViewLayout: layout);
        colView.contentInset = UIEdgeInsets(top: 156, left: 0, bottom: 0, right: 0)
        colView.showsVerticalScrollIndicator = false
//        colView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "homeCell")
        colView.delegate = self
        colView.dataSource = self
        colView.backgroundColor = UIColor.clear
        
        colView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: "CustomCollectionViewCell")
        colView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: "CardCollectionViewCell")
        
        colView.register(HeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        colView.register(FooterCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterCollectionReusableView")
        colView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "UICollectionReusableView")
        return colView
    }()

    @objc func loginIsTaped(tapGes:UITapGestureRecognizer) -> Void {
        // 1、获取当前label
        guard let currentLabel = tapGes.view as? UILabel else {
            return
        }
        let vc = LoginViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension ViewController4:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return itemAry0.count
        case 1:
            return 2
        case 2:
            return self.funArray!.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if(section==0 || section==1){
            return  CGSize(width: ScreenWidth-32, height: 44)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if(section==0 || section==1){
            return  CGSize(width: ScreenWidth-32, height: 16)
        }else{
            return CGSize(width: ScreenWidth-32, height: 120)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(indexPath.section==0 || indexPath.section==2){
            let width = (ScreenWidth-32)/3.0
            return CGSize(width: width, height: 82)
        }else{
            let width = (ScreenWidth-32)/2.0
            return CGSize(width: width, height: 72)
        }
    }
    


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as! CustomCollectionViewCell
            let ary = itemAry0[indexPath.item]
            cell.imageView?.image = UIImage.init(named:ary[0])
            cell.titleLabel!.text = ary[1]
            cell.backgroundColor = UIColor.white
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as! CustomCollectionViewCell
            let ary : Array = self.funArray![indexPath.item]
            cell.imageView?.image = UIImage.init(named:ary[0])
            cell.titleLabel!.text = ary[1]
            cell.backgroundColor = UIColor.white
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as! CardCollectionViewCell
            if(indexPath.item==0){
                cell.bakimgv?.image = UIImage(named: "money_background")
                cell.label2?.text = "我的钱包"
            }else{
                cell.bakimgv?.image = UIImage(named: "card_background")
                cell.label2?.text = "卡包"
                cell.bakimgv?.snp.makeConstraints({ (make) in
                    make.left.equalToSuperview()
                    make.right.equalToSuperview().offset(-20)
                })
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if(kind == UICollectionView.elementKindSectionHeader){
            let headerView :HeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            headerView.leftLabel?.isHidden = true
            headerView.rightLabel?.isHidden = true
            headerView.arrow?.isHidden = true;
            switch indexPath.section {
            case 0:
                headerView.leftLabel?.text = "我的订单"
                headerView.rightLabel?.text = "全部订单"
                headerView.leftLabel?.isHidden = false
                headerView.rightLabel?.isHidden = false
                headerView.arrow?.isHidden = false;
                return headerView;
            case 1:
                headerView.leftLabel?.text = "我的资产"
                headerView.leftLabel?.isHidden = false
            default:
                break
            }
            return headerView
        }else{
            if(indexPath.section == 0 || indexPath.section == 1){
                let footerView : UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "UICollectionReusableView", for: indexPath) as! UICollectionReusableView
                footerView.backgroundColor = UIColor.clear
                return footerView
            }
            let footerView : FooterCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
            footerView.btn?.addTarget(self, action: #selector(btnIsClicked(btn:)), for: .touchUpInside)
            if(self.isCustom==true){
                footerView.btn?.setTitle("当推荐官赚钱", for: .normal)
            }else{
                footerView.btn?.setTitle("切换到游客", for: .normal)
            }
            return footerView
        }
    }

    // 单击单元格时，跳转到详情页面
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.section==0){
            if(indexPath.item==0){
                let vc = NextViewController()
                vc.data1 = "hjx"
                vc.view.backgroundColor = UIColor.red
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
                let vc = TestViewController()
                self.navigationController?.pushViewController(vc, animated: true);
        }
    }
    
    @objc func btnIsClicked(btn:UIButton?) -> Void {
        self.isCustom! = !self.isCustom!
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
//        print(offsetY)
        if(offsetY < -120){
            self.view.bringSubviewToFront(nameLabel!)
            self.view.bringSubviewToFront(headerImageView!)
            self.view.bringSubviewToFront(mainImgv!)
        }else{
            self.view.sendSubviewToBack(nameLabel!)
            self.view.sendSubviewToBack(headerImageView!)
            self.view.sendSubviewToBack(mainImgv!)
            self.view.sendSubviewToBack(topImageView!)
            self.view.sendSubviewToBack(bgview!)
        }
    }
    
}

