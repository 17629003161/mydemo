//
//  LoginModel.swift
//  MyDemo
//
//  Created by mac on 2021/4/8.
//

import Foundation
import HandyJSON

// MARK: - Welcome
struct Welcome2:HandyJSON{
    var msg: String?
    var code: Int?
    var data: DataClass2?
}
struct DataClass2: HandyJSON {
    var token: String?
    var userId: Int?
}




struct Welcome1:Codable{
    let msg: String
    let code: Int
    let data: Bool
}


// MARK: - Welcome
struct Welcome: Codable {
    let msg: String
    let code: Int
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let token: String
    let userID: Int

    enum CodingKeys: String, CodingKey {
        case token
        case userID = "userId"
    }
}

