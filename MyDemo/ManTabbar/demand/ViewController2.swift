//
//  ViewController2.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import UIKit
import Alamofire

class ViewController2: LKBaseViewController,AMapSearchDelegate {
    var search : AMapSearchAPI!
    var request : AMapDistanceSearchRequest?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.view.backgroundColor = color
        self.view.backgroundColor = UIColor(hex: 0x00ff00)
        view .addSubview(mapView)
        test3()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
    
    func test2() -> Void {
        let r = MAUserLocationRepresentation()
        r.showsAccuracyRing = false
        r.showsHeadingIndicator = false
        r.fillColor = UIColor.red
        r.strokeColor = UIColor.blue
        r.lineWidth = 2
        r.enablePulseAnnimation = false
        r.locationDotBgColor = UIColor.green
        r.locationDotFillColor = UIColor.gray
        //r.image = UIImage(named: "shouji")
        
        mapView.update(r)
        
        mapView.showsCompass = false
        mapView.showsScale = false
        
        mapView.setZoomLevel(16.5, animated: true)
        
    }
    
    func test3() -> Void {
        request = AMapDistanceSearchRequest()
        request!.origins = [AMapGeoPoint.location(withLatitude: 39.989643, longitude: 115.481028)]
        request!.destination = AMapGeoPoint.location(withLatitude: 40.004717, longitude: 114.465302)
        request!.type = .drive
        search = AMapSearchAPI()
        search.delegate = self
        search .aMapDistanceSearch(request)
    }
    
    
    func onDistrictSearchDone(_ request: AMapDistrictSearchRequest!, response: AMapDistrictSearchResponse!) {
        print("\(response!)")
    }
    
    func aMapSearchRequest(_ request: Any!, didFailWithError error: Error!) {
        print("\(error)")
    }

}



