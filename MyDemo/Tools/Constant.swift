//
//  Constant.swift
//  MyDemo
//
//  Created by mac on 2021/2/24.
//

import Foundation
import UIKit

let ScreenBounds = UIScreen.main.bounds
let ScreenWidth = UIScreen.main.bounds.size.width
let ScreenHeight = UIScreen.main.bounds.size.height
let ScaleWidth = ScreenWidth / 375.0
let ScaleHeight = ScreenHeight / 667.0
let ScaleSize = ScreenWidth > 1 ? ScreenWidth : 1
let BaseUrl = "https://www.sxlkcx.cn/travel/"

let StatusBarHeight = UIApplication.shared.statusBarFrame.size.height >= 44 ? UIApplication.shared.statusBarFrame.size.height : 20
let NavigationBarHeight = StatusBarHeight + 44
let TabBarHeight: CGFloat = StatusBarHeight > 20 ? 83 : 49
let BottomBarHeight: CGFloat = StatusBarHeight > 20 ? 34 : 0

let ISiPhoneX = StatusBarHeight > 20 ? true : false
let ISiPhoneSE = ScreenWidth < 375 ? true : false

let TimeIntervalRequest = TimeInterval(20)
let TimeIntervalAnimation = TimeInterval(0.25)
let TimeIntervalAnimationFast = TimeInterval(0.15)


struct KEY {
    
    static let bootpage = "bootpage"
    
}


struct NotificationName {
    
    static let getNetworking = NSNotification.Name(rawValue:"getNetworking")
    static let loseNetworking = NSNotification.Name(rawValue:"loseNetworking")
    
}


struct Tag {
    static let advert = 900
    static let banner = 901
    static let load = 990
    
    static let loading = 998
    static let toast = 999
}


extension UIFont {
    
    static var large = UIFont.defaultFont(size: 20)
    static var big = UIFont.defaultFont(size: 18)
    static var title = UIFont.defaultFont(size: 17)
    static var standard = UIFont.defaultFont(size: 16)
    static var normal = UIFont.defaultFont(size: 14)
    static var small = UIFont.defaultFont(size: 12)
    
    static var largeMedium = UIFont.mediumFont(size: 20)
    static var bigMedium = UIFont.mediumFont(size: 18)
    static var titleMedium = UIFont.mediumFont(size: 17)
    static var standardMedium = UIFont.mediumFont(size: 16)
    static var normalMedium = UIFont.mediumFont(size: 14)
    
    static var largeBold = UIFont.boldFont(size: 20)
    static var bigBold = UIFont.boldFont(size: 18)
    static var titleBold = UIFont.boldFont(size: 17)
    static var standardBold = UIFont.boldFont(size: 16)
    
    class func defaultFont(size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    
    class func mediumFont(size: CGFloat) -> UIFont {
        return UIFont(name: "PingFangSC-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func boldFont(size: CGFloat) -> UIFont {
        return UIFont.boldSystemFont(ofSize: size)
    }
    
}
