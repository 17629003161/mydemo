//
//  LKLocationManager.swift
//  MyDemo
//
//  Created by mac on 2021/3/11.
//

import UIKit

class LKLocationManager: NSObject,AMapLocationManagerDelegate {
    let locationManager = AMapLocationManager();
    static let sharedManager = LKLocationManager()
    private override init() {
    }
    
    func startLocation() -> Void {
        locationManager.delegate = self
        locationManager.distanceFilter = 200
        //iOS 9（不包含iOS 9） 之前设置允许后台定位参数，保持不会被系统挂起
        locationManager.pausesLocationUpdatesAutomatically = false
        //iOS 9（包含iOS 9）之后新特性：将允许出现这种场景，同一app中多个locationmanager：一些只能在前台定位，另一些可在后台定位，并可随时禁止其后台定位。
        if UIDevice.current.systemVersion._bridgeToObjectiveC().doubleValue >= 9.0 {
            locationManager.pausesLocationUpdatesAutomatically = true
        }
        locationManager.startUpdatingLocation()
        locationManager.locatingWithReGeocode = true
        locationManager.startUpdatingLocation()
    }
    
    func stopLocation() -> Void {
        locationManager.stopUpdatingLocation()
    }
    
    func amapLocationManager(_ manager: AMapLocationManager!, didUpdate location: CLLocation!, reGeocode: AMapLocationReGeocode?) {
        NSLog("location:{lat:\(location.coordinate.latitude); lon:\(location.coordinate.longitude); accuracy:\(location.horizontalAccuracy);};");
        
        if let reGeocode = reGeocode {
            NSLog("reGeocode:%@", reGeocode)
        }
    }
    
    
}
