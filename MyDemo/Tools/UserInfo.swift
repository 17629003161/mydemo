//
//  UserInfo.swift
//  MyDemo
//
//  Created by mac on 2021/3/11.
//

import UIKit

class UserInfo: NSObject {
    var userId : String?
    var name : String?
    var sex : String?
    
    static let sharedInstance = UserInfo()
    private override init() {
    }
}
